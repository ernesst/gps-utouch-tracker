#!/usr/bin/env python3


#Copyright (C) 2019 Ernesst <ernesst@posteo.net>
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; version 3.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# 0.1 intial release 
# 0.2 update UI

import time
import subprocess
import datetime
import re
import json
import time
import sys
import json
import requests
import os
import multiprocessing
import sys
sys.path.insert(0, "./gpxpy-1.3.5/")
import gpxpy
import gpxpy.gpx
import shutil
import math as mod_math

release = "0.2"
Freq = 2.5

i = 0
gpx = gpxpy.gpx.GPX()

# Create first track in our GPX:
gpx_track = gpxpy.gpx.GPXTrack()
gpx.tracks.append(gpx_track)

# Create first segment in our GPX track:
gpx_segment = gpxpy.gpx.GPXTrackSegment()
gpx_track.segments.append(gpx_segment)


def format_time(time_s):
    if not time_s:
        return 'n/a'
    else:
        minutes = mod_math.floor(time_s / 60.)
        hours = mod_math.floor(minutes / 60.)
        return '%s:%s:%s' % (str(int(hours)).zfill(2), str(int(minutes % 60)).zfill(2), str(int(time_s % 60)).zfill(2))


def format_long_length(length):
    return '{:.3f}km'.format(length / 1000.)


def format_short_length(length):
    return '{:.1f}m'.format(length)

def format_speed(speed):
    if not speed:
        speed = 0
    else:
        return '{:.0f}m/s = {:.0f}km/h'.format(speed, speed * 3600. / 1000.)
def read_gps():
	global latitude
	global longitude
	global accuracy
	global elevation
	latitude = str("")
	longitude = str("")
	accuracy = str("")
	elevation_a = []
	elevation = str("")
	cmd = ['sudo test_gps']
	p = subprocess.Popen(cmd, stdout = subprocess.PIPE,
	stderr = subprocess.STDOUT, shell = True)
	for line in p.stdout:
		line = line.decode('utf-8')
		if re.search("^latitude", line):
				latitude = line.split()
				latitude = latitude[1]

		if re.search("^longtide", line): #bug in test_gps
				longitude = line.split()
				longitude = longitude[1]

		if re.search("^accuracy", line): #bug in test_gps
				accuracy = line.split()
				accuracy= accuracy[1]

		if re.search("elevation", line): #bug in test_gps
				elevation_T = line.split()
				#print(elevation)
				elevation_a.append(elevation_T[1])
				#print(elevation)
		if accuracy != "":
			#print(line + "=> accuracy : "  + accuracy)
			#print(line + "=> longitude : "  + longitude)
			#print(line + "=> latitude : "  + latitude)
			for i in range(len(elevation_a)):
				elevation_a[i] = float(elevation_a[i])
			elevation = int(sum(elevation_a) / float(len(elevation_a)))
			p.kill()
			return elevation,longitude,latitude

try:
	os.system('clear')
	print("\n**********************************")
	print("** UTouch GPS Tracking tool "+ str(release)+" **")
	print("**********************************")
	print("The tracking will start automatically.")
	print("To close the application simply select Ctrl+c virtual key, the data will be automatically saved into a .gpx file named as per current timestamp.")
	print("\nCurrent tracking period : " + str(Freq) +"s")
	print("Enjoy !")
	print("\n")
	while True:
		read_gps()
		os.system('clear')
		print("\n**********************************")
		print("** UTouch GPS Tracking tool "+ str(release)+" **")
		print("**********************************")
		print("To close the application simply select Ctrl+c virtual key")
		print("\nPositioning No : " + str(i))
		print("lat : " + latitude + " lon : " + longitude)
		gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(latitude, longitude, elevation=elevation, time=datetime.datetime.utcnow()))
		time.sleep(Freq)
		i = i + 1

except KeyboardInterrupt:
	filename = str(round(time.time() * 1000))
	os.system('clear')
	print('\nGPX file Created : ' + filename + ".gpx")
	file = open(filename + ".gpx","w+")
	file.write(gpx.to_xml())
	file.close()
	gpx_file = filename + ".gpx"
	shutil.chown(gpx_file, user="phablet", group="phablet")
	gpx = gpxpy.parse(open(gpx_file))
	indentation='    '
	info_display = ""
	"""
	gpx_part may be a track or segment.
	"""
	length_2d = gpx.length_2d()
	length_3d = gpx.length_3d()
	info_display += "\n%sLength 2D: %s" % (indentation, format_long_length(length_2d))
	info_display += "\n%sLength 3D: %s" % (indentation, format_long_length(length_3d))
	moving_time, stopped_time, moving_distance, stopped_distance, max_speed = gpx.get_moving_data()
	info_display += "\n%sMoving time: %s" %(indentation, format_time(moving_time))
	info_display += "\n%sStopped time: %s" %(indentation, format_time(stopped_time))
	info_display += "\n%sMax speed: %s" % (indentation, format_speed(max_speed))
	info_display += "\n%sAvg speed: %s" % (indentation, format_speed(moving_distance / moving_time) if moving_time > 0 else "?")
	uphill, downhill = gpx.get_uphill_downhill()
	info_display += "\n%sTotal uphill: %s" % (indentation, format_short_length(uphill))
	info_display += "\n%sTotal downhill: %s" % (indentation, format_short_length(downhill))
	info_display += "\n\n\n"
	print(info_display)
	#print(gpx)
	sys.exit()
